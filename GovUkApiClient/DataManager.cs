﻿using System;
using System.Net;
using System.IO;

namespace GovUkApiClient
{
    public class DataManager
    {
        const string GovUkApiUri = "https://www.gov.uk/api/content/";

        public static string GetRequest(string url)
        {
            try
            {
                var webRequest = WebRequest.Create(GovUkApiUri + url) as HttpWebRequest;
                webRequest.Method = WebRequestMethods.Http.Get;
                webRequest.Accept = "application/json";

                var response = webRequest.GetResponse() as HttpWebResponse;

                Stream responseStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(responseStream);

                String content = readStream.ReadToEnd();

                response.Close();
                responseStream.Close();
                readStream.Close();

                return content;

            }
            catch (Exception e)
            {
                FailComponent(e.ToString());
                return e.ToString();
            }
        }

        public static string PostRequest(string url, string json)
        {
            try
            {
                var webRequest = WebRequest.Create(GovUkApiUri + url) as HttpWebRequest;
                webRequest.Method = WebRequestMethods.Http.Post;
                webRequest.ContentType = "application/json; charset=UTF-8";

                using (var streamWriter = new StreamWriter(webRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }

                var response = webRequest.GetResponse() as HttpWebResponse;
                using (var streamReader = new StreamReader(response.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    return result;
                }

            }
            catch (WebException e)
            {
                string err = e.Message + "/" + ((HttpWebResponse)e.Response).StatusCode + "/" + ((HttpWebResponse)e.Response).StatusDescription;
                FailComponent(err);
                return err;
            }
            catch (Exception e)
            {
                FailComponent(e.ToString());
                return e.ToString();
            }
        }

        private static string FailComponent(string errorMsg)
        {
            return "{'ErrorMessage': '" + errorMsg + "'}";
        }
    }
}
