﻿using System;
using Newtonsoft.Json.Linq;
using System.Windows.Forms;
using UKGov;

namespace GovUkApiClient
{
    public partial class GovUkApiClient : Form
    {
        UKGovClient client;
        public GovUkApiClient()
        {
            InitializeComponent();

            client = new UKGovClient();
        }

        private void GovUkApiClient_Load(object sender, EventArgs e)
        {
        }

        String GetData(string url)
        {
            try
            {
                //JToken content = JToken.Parse(client.GetRequest(url, "text/html"));
                string formatted = client.GetRequest(url);
               // string formatted = content.ToString(Newtonsoft.Json.Formatting.Indented);
                return formatted;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private void btnRequest_Click(object sender, EventArgs e)
        {
            tbResponse.Text = GetData(tbUrl.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tbUrl.Text = String.Empty;
            tbResponse.Text = String.Empty;

        }

        private void tbResponse_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
